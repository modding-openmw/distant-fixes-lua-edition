## Testing Distant Fixes: Lua Edition

**SPOILERS BELOW**

How to test this mod.

#### Aether Pirate's Discovery

1. Run the game
1. Press \` to bring down the console
1. Type `coe 11 3` and press enter
1. Press \` again to close the console
1. Look to the east, you should be able to see Tel Fyr and note there is no aethership floating near it
1. Press \` to bring down the console
1. Type `coc "ald velothi"` and press enter
1. Type `tcl"` and press enter (if you've not already disable collision in this way)
1. Press \` again to close the console
1. Look west, you should see the aethership above a Dwemer ruin
1. Move towards the aethership and talk to the NPC standing on it
1. Advance the quest until you charge the Dwemer coherer and bring it back to the NPC on the aethership
1. At this point, the next dialogue choice will move the aethership and everyone on it over to Tel Fyr
1. Press \` to bring down the console
1. Type `coc "ald velothi"` and press enter
1. Press \` again to close the console
1. Look west, you should no longer see the aethership above the dwemer ruin

#### Astrologian's Guild

This is a very large mod so only general steps will be provided.

1. Run the game
1. Fly near to Hlormaren (position yourself at a great height to notice no oblivion gate-like objects just north of it, nor should you see a partially-sunken Dwemer ruin)
1. Fly to Rayna Drolan's Shack, look north to see where the guild hall should be (it should not be there, just an island)
1. Press \` to bring down the console
1. Type `set ag_guildhall to 1` and press enter
1. Press \` again to close the console
1. Notice the distant objects change
1. Repeat the above steps but swap out `1` for `2`, `3`, up to `6`

#### Great House Strongholds

1. Run the game
1. Go some place with a great house stronghold in view but not so close that you load it
1. Press \` to bring down the console
1. Type `Journal HT_Stronghold 55` and press enter
1. Type `set Stronghold to 1` and press enter
1. Press \` again to close the console
1. After a few moments the stronghold construction will advance
1. Press \` to bring down the console
1. Type `Journal HT_Stronghold 100` and press enter
1. Type `set Stronghold to 2` and press enter
1. Press \` again to close the console
1. After a few moments the stronghold construction will advance
1. Press \` to bring down the console
1. Type `Journal HT_Stronghold 200` and press enter
1. Type `set Stronghold to 4` and press enter
1. Press \` again to close the console
1. After a few moments the stronghold construction will advance
1. Press \` to bring down the console
1. Type `Journal HT_Stronghold 200` and press enter
1. Type `set Stronghold to 6` and press enter
1. Press \` again to close the console
1. After a few moments the stronghold construction will advance

Note that skipping `3` and `5` for the `Stronghold` variable is not an error.

#### Ghostfence

1. Run the game
1. Go some place with ghostfence in view but not so close that you load it
1. Press \` to bring down the console
1. Type `Journal C3_DestroyDagoth 20` and press enter
1. Press \` again to close the console
1. After a few moments the ghostfence pieces will disable
