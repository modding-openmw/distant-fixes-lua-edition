#!/bin/sh
set -eu

#
# This script packages the project into a zip file.
#

file_name=distant-fixes-lua-edition.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags || git rev-parse --short HEAD)
EOF

zip --must-match --recurse-paths ${file_name} data scripts CHANGELOG.md l10n LICENSE README.md TESTING.md distant-fixes-lua-edition.omwscripts version.txt
sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
