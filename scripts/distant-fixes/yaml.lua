local core = require("openmw.core")
local markup = require('openmw.markup')
local vfs = require('openmw.vfs')
local common = require("scripts.distant-fixes.common")

-- Read YAML data
local fixes = {}
local startup = {}
local update = {}
for ymlfile in vfs.pathsWithPrefix("data/distant-fixes") do
    local yml = markup.loadYaml(ymlfile)
    if yml == nil then
        print("[DistantFixes]: [WARNING] Malformed or no data in file: ", ymlfile)
        goto continue
    end
    for _, fix in pairs(yml) do
        table.insert(fixes, fix)
    end
    ::continue::
end
common.msg("All YAML data processed")

-- Store data for use
local gotError = false
for _, fix in pairs(fixes) do
    if core.contentFiles.has(fix.plugin) then
        local haveStartup = fix.fixes.startup ~= nil and #fix.fixes.startup > 0
        local haveUpdate = fix.fixes.update ~= nil and #fix.fixes.update > 0
        if haveStartup then
            for _, data in pairs(fix.fixes.startup) do
                local activators = {}
                local doors = {}
                local lights = {}
                local withScript = {}
                if data.before == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'before' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.before = "missing"
                end
                if data.cellX == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'cellX' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.cellX = "missing"
                end
                if data.cellY == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'cellY' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.cellY = "missing"
                end
                if data.enabled == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'enabled' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.enabled = "missing"
                end
                if data.questId == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'questID' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.questId = "missing"
                end
                if data.activators ~= nil then
                    for _, recordId in pairs(data.activators) do
                        activators[string.lower(recordId)] = true
                    end
                end
                if data.doors ~= nil then
                    for _, recordId in pairs(data.doors) do
                        doors[string.lower(recordId)] = true
                    end
                end
                if data.lights ~= nil then
                    for _, recordId in pairs(data.lights) do
                        lights[string.lower(recordId)] = true
                    end
                end
                if data.withScript ~= nil then
                    for _, script in pairs(data.withScript) do
                        withScript[string.lower(script)] = true
                    end
                end
                if data.withScript == nil and (data.lights == nil and data.doors == nil and data.activators == nil) then
                    print("[DistantFixes]: ERROR: You must provide a list of objects to act upon!")
                    print("[DistantFixes]: ERROR: Either activators, doors, or lights must be provided!")
                    print("[DistantFixes]: ERROR: Or you may provide an array of script names!")
                    print(string.format("[DistantFixes]: ERROR: (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                end
                data.mod = fix.mod
                data.plugin = fix.plugin
                local activatorsCount
                local doorsCount
                local lightsCount
                local scriptCount
                if data.activators == nil then
                    activatorsCount = 0
                else
                    activatorsCount = #data.activators
                end
                if data.doors == nil then
                    doorsCount = 0
                else
                    doorsCount = #data.doors
                end
                if data.lights == nil then
                    lightsCount = 0
                else
                    lightsCount = #data.lights
                end
                if data.withScript ~= nil then
                    scriptCount = #data.withScript
                end
                data.activators = activators
                data.doors = doors
                data.lights = lights
                data.withScript = withScript
                data.scriptCount = scriptCount
                -- example key: MorrowindMorrowind.esmHT_Stronghold55101false136125
                data.key = data.mod .. data.plugin .. data.questId .. data.before .. data.cellX .. data.cellY .. tostring(data.enabled) .. tostring(activatorsCount) .. tostring(doorsCount) .. tostring(lightsCount)
                if scriptCount then
                    data.key = data.key .. scriptCount
                end
                table.insert(startup, data)
            end
        end
        if haveUpdate then
            for _, data in pairs(fix.fixes.update) do
                local activators = {}
                local doors = {}
                local lights = {}
                local withScript = {}
                if data.cellX == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'cellX' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.cellX = "missing"
                end
                if data.cellY == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'cellY' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.cellY = "missing"
                end
                if data.enabled == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'enabled' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.enabled = "missing"
                end
                if data.questId == nil then
                    print(string.format("[DistantFixes]: ERROR: The 'questID' field is required! (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                    data.questId = "missing"
                end
                if data.activators ~= nil then
                    for _, recordId in pairs(data.activators) do
                        activators[string.lower(recordId)] = true
                    end
                end
                if data.doors ~= nil then
                    for _, recordId in pairs(data.doors) do
                        doors[string.lower(recordId)] = true
                    end
                end
                if data.lights ~= nil then
                    for _, recordId in pairs(data.lights) do
                        lights[string.lower(recordId)] = true
                    end
                end
                if data.withScript ~= nil then
                    for _, script in pairs(data.withScript) do
                        withScript[string.lower(script)] = true
                    end
                end
                if data.withScript == nil and (data.lights == nil and data.doors == nil and data.activators == nil) then
                    print("[DistantFixes]: ERROR: You must provide a list of objects to act upon!")
                    print("[DistantFixes]: ERROR: Either activators, doors, or lights must be provided!")
                    print("[DistantFixes]: ERROR: Or you may provide an array of script names!")
                    print(string.format("[DistantFixes]: ERROR: (%s/%s)", fix.mod, fix.plugin))
                    gotError = true
                end
                data.mod = fix.mod
                data.plugin = fix.plugin
                local activatorsCount
                local doorsCount
                local lightsCount
                local scriptCount
                if data.activators == nil then
                    activatorsCount = 0
                else
                    activatorsCount = #data.activators
                end
                if data.doors == nil then
                    doorsCount = 0
                else
                    doorsCount = #data.doors
                end
                if data.lights == nil then
                    lightsCount = 0
                else
                    lightsCount = #data.lights
                end
                if data.withScript ~= nil then
                    scriptCount = #data.withScript
                end
                data.activators = activators
                data.doors = doors
                data.lights = lights
                data.withScript = withScript
                data.scriptCount = scriptCount
                -- example key: Aether Pirate's DiscoveryAether Pirate's Discovery.omwaddonvmj_quest115-1314false520
                data.key = data.mod .. data.plugin .. data.questId .. data.stage .. data.cellX .. data.cellY .. tostring(data.enabled) .. tostring(activatorsCount) .. tostring(doorsCount) .. tostring(lightsCount)
                if scriptCount then
                    data.key = data.key .. scriptCount
                end
                table.insert(update, data)
            end
        end
        if gotError == false and (haveStartup or haveUpdate) then
            print(string.format("[DistantFixes]: Enabling support for: %s (%s)", fix.mod, fix.plugin))
        end
    end
end

if gotError == true then
	error("[Distant Fixes]: Lua Edition could not start up due to YAML data problems. See above for relevant errors!")
end

return {
    startup = startup,
    update = update
}
