local core = require("openmw.core")
local self = require("openmw.self")
local I = require("openmw.interfaces")

local common = require("scripts.distant-fixes.common")

local questData = {}

I.Settings.registerPage {
    key = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "name",
    description = "description"
}

local function receiveQuestData(d)
    for _, data in pairs(d) do
        local q = string.lower(data.questId)
        common.msg(string.format("Quest data received for: %s (%s)", data.questId, data.key))
        if questData[q] == nil then
            questData[q] = {data}
        else
            table.insert(questData[q], data)
        end
    end
end

local function onQuestUpdate(questId, stage)
    if questData[questId] == nil then return end
    for _, data in pairs(questData[questId]) do
        if data == nil then return end
        if stage == data.stage then
            common.msg(string.format("Handling quest update: %s; stage: %s", questId, stage))
            data.player = self
            core.sendGlobalEvent("momw_dfl_runScriptCheck", data)
        end
    end
end

return {
    engineHandlers = {onQuestUpdate = onQuestUpdate},
    eventHandlers = {momw_dfl_receiveQuestData = receiveQuestData}
}
