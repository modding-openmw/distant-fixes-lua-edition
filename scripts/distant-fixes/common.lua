local async = require("openmw.async")
local storage = require("openmw.storage")
local MOD_ID = "DistantFixes"
local settingsGlobal = "SettingsGlobal" .. MOD_ID
local modSettings = storage.globalSection(settingsGlobal)

local debugOn = modSettings:get("debugOn")
local function updateDebugs(_, key)
    if key == "debugOn" then
        debugOn = modSettings:get("debugOn")
    end
end
modSettings:subscribe(async:callback(updateDebugs))

local function msg(...)
    if debugOn then
        print("[DistantFixes]: " .. ...)
    end
end

return {
    MOD_ID = MOD_ID,
    msg = msg,
    scriptCheckInterval = .5,
    settingsGlobal = settingsGlobal
}
