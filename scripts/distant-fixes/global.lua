local aux_util = require("openmw_aux.util")
local time = require("openmw_aux.time")
local types = require("openmw.types")
local world = require("openmw.world")
local I = require("openmw.interfaces")

local common = require("scripts.distant-fixes.common")
local yaml = require("scripts.distant-fixes.yaml")

local typeMap = {
    [types.Activator] = "activators",
    [types.Door] = "doors",
    [types.Light] = "lights"
}

local completedStartup = {}
local stopScriptCheck = {}

I.Settings.registerGroup {
    key = common.settingsGlobal,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "modSettingsTitle",
    description = "modSettingsDesc",
    permanentStorage = false,
    settings = {
        {
            key = "debugOn",
            name = "debugOn_name",
            description = "debugOn_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local function handleObjects(data)
    local playerCell = world.players[1].cell
    if playerCell.gridX == data.cellX and playerCell.gridY == data.cellY then
        common.msg(string.format("Skipping key because player is in the same cell: %s", data.key))
        return false
    end
    for handledType, _ in pairs(typeMap) do
        for _, obj in pairs(world.getExteriorCell(data.cellX, data.cellY):getAll(handledType)) do
            if
                -- This thing has a script we care about
                data.withScript[obj.type.record(obj).mwscript]
                -- This thing is a specific thing we care about
                or data[typeMap[handledType]][string.lower(obj.recordId)]
            then
                common.msg(string.format("Setting enabled for \"%s\": %s", obj.recordId, data.enabled))
                obj.enabled = data.enabled
            end
        end
    end
    return true
end

local function endScriptCheck(key, justStop)
    stopScriptCheck[key].func()
    stopScriptCheck[key].func = nil
    common.msg(string.format("Stopped func for: %s", key))
    if justStop == nil then
        completedStartup[key] = {mod = stopScriptCheck[key].data.mod, plugin = stopScriptCheck[key].data.plugin}
        -- Empty out data since it's not needed and will bloat save files
        stopScriptCheck[key] = nil
        common.msg(string.format("Recorded key as done: %s", key))
    end
end

local function runScriptCheck(data)
    if stopScriptCheck[data.key] == nil then
        stopScriptCheck[data.key] = {}
        stopScriptCheck[data.key].data = data
        stopScriptCheck[data.key].done = false
    end
    stopScriptCheck[data.key].func = time.runRepeatedly(
        function()
            -- This function has to handle several different scenarios, all of
            -- which are explained below within each if branch.
            if stopScriptCheck[data.key].done == true then return end
            if
                data.cellX ~= nil
                and data.cellY ~= nil
                and data.script ~= nil
                and data.var ~= nil
                and data.val ~= nil
            then
                -- Disable/enable by checking for a script, a variable within, and its value
                local script = world.mwscript.getGlobalScript(string.lower(data.script))
                if script and script.isRunning and script.variables[string.lower(data.var)] == data.val then
                    if handleObjects(data) then
                        endScriptCheck(data.key)
                    end
                end

            elseif
                data.cellX ~= nil
                and data.cellY ~= nil
                and data.script ~= nil
                and data.var == nil
                and data.val == nil
            then
                -- Disable/enable by checking for just a script
                local script = world.mwscript.getGlobalScript(string.lower(data.script))
                if script and script.isRunning and script.variables then
                    if handleObjects(data) then
                        endScriptCheck(data.key)
                    end
                end

            elseif
                data.cellX ~= nil
                and data.cellY ~= nil
                and data.globalVar ~= nil
                and data.globalVar.key ~= nil
                and data.globalVar.value ~= nil
                and data.player ~= nil
            then
                -- Disable/enable by checking a global variable and its value
                if world.mwscript.getGlobalVariables(data.player)[data.globalVar.key] == data.globalVar.value then
                    if handleObjects(data) then
                        endScriptCheck(data.key)
                    end
                end
            elseif data.cellX ~= nil and data.cellY ~= nil then
                -- Disable/enable just because a quest updated; all we need are cell X/Y
                if handleObjects(data) then
                    endScriptCheck(data.key)
                end
            else
                --TODO: This shouldn't ever happen since we blow up when reading YAML that lacks this data
                print("[DistantFixes]: WARNING!")
                print("[DistantFixes]: WARNING!")
                print("[DistantFixes]: WARNING! No objects were processed with the given data (this is probably an error in your data):")
                print("[DistantFixes]: WARNING!")
                print("[DistantFixes]: WARNING!")
                print(aux_util.deepToString(data, 10))
                endScriptCheck(data.key, true)
            end
        end,
        common.scriptCheckInterval,
        {
            initialDelay = 1,
            type = time.SimulationTime
        }
    )
end

local function onLoad(data)
    if data == nil then return end
    completedStartup = data.completedStartup
    stopScriptCheck = data.stopScriptCheck
    -- Resume any stopped functions
    for _, stored in pairs(stopScriptCheck) do
        common.msg(string.format("Resuming stopped func for: %s", stored.data.key))
        runScriptCheck(stored.data)
    end
end

local function onSave()
    -- Functions are not serializable, therefor not saveable
    for key, stored in pairs(stopScriptCheck) do
        if stored.func ~= true then
            common.msg(string.format("Pausing func for: %s", stored.data.key))
            stored.func()
            stopScriptCheck[key].func = nil
        end
    end
    return {
        completedStartup = completedStartup,
        stopScriptCheck = stopScriptCheck
    }
end

local function onPlayerAdded(player)
    player:sendEvent("momw_dfl_receiveQuestData", yaml.update)
    for _, data in pairs(yaml.startup) do
        if completedStartup[data.key] then goto continue end
        if player.type.quests(player)[data.questId].stage < data.before then
            handleObjects(data)
            common.msg(string.format("Marking startup key as completed: %s", data.key))
            completedStartup[data.key] = {mod = data.mod, plugin = data.plugin}
        end
        ::continue::
    end
end

local function showCompleted()
    print(string.format("[%s]:", common.MOD_ID))
    print(string.format("[%s]: Showing completed:", common.MOD_ID))
    print(string.format("[%s]:", common.MOD_ID))
    print(aux_util.deepToString(completedStartup, 4))
    print(string.format("[%s]:", common.MOD_ID))
    print(string.format("[%s]: End completed:", common.MOD_ID))
    print(string.format("[%s]:", common.MOD_ID))
end

local function showCurrent()
    print(string.format("[%s]:", common.MOD_ID))
    print(string.format("[%s]: Showing current:", common.MOD_ID))
    print(string.format("[%s]:", common.MOD_ID))
    for _, data in pairs(stopScriptCheck) do
        print(aux_util.deepToString(data, 10))
    end
    print(string.format("[%s]:", common.MOD_ID))
    print(string.format("[%s]: End current:", common.MOD_ID))
    print(string.format("[%s]:", common.MOD_ID))
end

return {
    engineHandlers = {
        onLoad = onLoad,
        onPlayerAdded = onPlayerAdded,
        onSave = onSave
    },
    eventHandlers = {momw_dfl_runScriptCheck = runScriptCheck},
    interfaceName = common.MOD_ID,
    interface = {
        ShowCompleted = showCompleted,
        ShowCurrent = showCurrent,
        version = 1
    }
}
