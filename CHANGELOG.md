## Distant Fixes: Lua Edition Changelog

#### Version 1.1

* Added support for several mods (credit Ronik):
  * [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545)
  * [The Archmagister's Abode](https://www.nexusmods.com/morrowind/mods/53376)
  * [Uvirith's Legacy](https://modding-openmw.com/mods/uviriths-legacy/)
  * [Uvirith's Manor](https://www.nexusmods.com/morrowind/mods/44135/)
  * [Building Up Uvirith's Legacy](https://modding-openmw.com/mods/building-up-uviriths-legacy/)
* Fixed a bug that caused an error when a quest updated that had no data to process
* Added the `withScript` field for fixes
    * This is a list (array) of strings that are names of scripts
    * If an object has one of the scripts in the list attached to it, the fix will handle it
    * This makes supporting content with large amounts of changes much easier (Raven Rock, Building Up Uvirith's Legacy)

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-lua-edition/-/packages/30585571)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/distant-fixes-lua-edition/-/packages/26628859)
