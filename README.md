# Distant Fixes: Lua Edition

**Requires OpenMW 0.49 or newer!**

A Lua framework for handling updates to distant objects that can support any content via YAML-based data.

Support for new content can be added with zero code changes or updates to this one; any other mod that needs to have distant objects handled by this one can simply create a properly-formatted YAML file and include it in their mod (see [below](#how-to-add-support-for-your-mod) for more information).

#### How It Works

1. When the mod is loaded, files ending in `.yml` in the `data/distant-fixes` directory are read
1. If valid data is found (see [below](#how-to-add-support-for-your-mod)) it is processed and fixes are executed
1. "Startup" fixes will set things enabled or disabled as needed for a new game
1. "Update" fixes will do the same thing after a quest update and may depend on other conditions (see [below](#how-to-add-support-for-your-mod))
1. When a fix is executed, it's recorded as having ran
1. Likewise: when it's finished it is recorded as such
1. If the game is saved and loaded with active update fixes running, those will be paused and re-ran as needed
1. Any mod can ship its own data to be used by this one (see [below](#how-to-add-support-for-your-mod))

This can technically be added to an existing save since both startup and update fixes only happen for specific quest stages.

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* **Hemaris** for making [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)
* **ZackHasACat** for making [Dynamic Distant Buildings - OpenMW Lua](https://www.nexusmods.com/morrowind/mods/53214)
* **ezze** for being a great rubber duck ❤️
* **Gonzo** for helping out with the gif
* **The OpenMW team, including every contributor** for making OpenMW and OpenMW-CS
* **The Modding-OpenMW.com team** for being amazing
* **All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server as well as the showcase thread for this mod** for their dilligent testing and feedback ❤️
* **Bethesda** for making Morrowind

#### Supported Content

This is a work-in-progress!

* Morrowind
* Bloodmoon (Only startup fixes are provided at present; updates are a WIP)
* [Aether Pirate's Discovery](https://www.nexusmods.com/morrowind/mods/53320)
* [Astrologian's Guild](https://www.nexusmods.com/morrowind/mods/51216)
* [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231)
* [Building Up Uvirith's Legacy](https://modding-openmw.com/mods/building-up-uviriths-legacy/) (Only startup fixes are provided at present; updates are a WIP)
* [Rise of House Telvanni](https://www.nexusmods.com/morrowind/mods/27545)
* [The Archmagister's Abode](https://www.nexusmods.com/morrowind/mods/53376)
* [Uvirith's Legacy](https://stuporstar.sarahdimento.com/) (including the Rise of House Telvanni add-on)
* [Uvirith's Manor](https://www.nexusmods.com/morrowind/mods/44135)

The following mods provide the same feature and **should not be used with this one**:

* [Dynamic Distant Buildings for OpenMW](https://www.nexusmods.com/morrowind/mods/51236)
* [Dynamic Distant Buildings - OpenMW Lua](https://www.nexusmods.com/morrowind/mods/53214)
* [Distant Fixes: Rise of House Telvanni](https://modding-openmw.gitlab.io/distant-fixes-rise-of-house-telvanni/)
* [Distant Fixes: Uvirith's Legacy](https://modding-openmw.gitlab.io/distant-fixes-uviriths-legacy/)
* [Distant Fixes: Uviriths Manor](https://modding-openmw.gitlab.io/distant-fixes-uviriths-manor/)

Know of content that needs to be supported? Please suggest it [on our GitLab issue tracker](https://gitlab.com/modding-openmw/distant-fixes-lua-edition/-/issues)!

#### How To Add Support For Your Mod

Support for additional content can me included with any 3rd party mod and need not be included in this one.

At a high level, this mod needs to know a few things in order to do work:

1. What gets affected?
1. Where is it?
1. When shall it be affected?
1. How is it affected (enable/disable)?

This mod works by providing a text-based data format that you can use to provide the above information.

##### Data Examples

Below is an explanation for the various ways you can trigger a distant fix for your mod:

1. Create a file at `data/distant-fixes/your-mod-name-here.yml`, change `your-mod-name-here` as desired.
1. The file must include data for this mod to process, several scenarios are supported:
    1. Startup: enable or disable things in a new game (or possibly when added to an existing save):

    ```yaml
    - mod: "Your Mod Name Here"
      plugin: "Your Mod.esp"
      fixes:
        startup: # NOTE this "startup" key is important!
          - questId: "Quest_ID_Here"
            # Stage of the quest to check for
            stage: 15
            # Cell X/Y where this should happen
            cellX: -123
            cellY: 23
            # Enable or disable  the thing/things
            enabled: false
            # List of activator IDs to enable or disable (optional)
            activators:
              - "obj_id_act1"
              - "obj_id_act2"
            # List of door IDs to enable or disable (optional)
            doors:
              - "obj_id_door1"
              - "obj_id_door2"
            # List of light IDs to enable or disable (optional)
            lights:
              - "obj_id_light1"
              - "obj_id_light2"
            # List of script IDs to check objects for
            withScript:
              - "SomeScript_1"
              - "SomeScript_2"
              - "SomeOtherScript_3"
        update:
          # ...Stuff...
    ```

    2. Check for a script, a variable within, and its value after a quest update:

    ```yaml
    - mod: "Your Mod Name Here"
      plugin: "Your Mod.esp"
      fixes:
        startup: 
          # ...Stuff...
        update: # NOTE this "update" key is important!
          - questId: "Quest_ID_Here"
            # Stage of the quest to check for
            stage: 15
            # Cell X/Y where this should happen
            cellX: -123
            cellY: 23
            # Enable or disable  the thing/things
            enabled: false
            # The script to look for after the given quest stage
            script: "Script_ID_Here"
            # Name of a variable to check for in the script (optional)
            var: "doOnce"
            # Value of the variable to check for (optional; required if var is given)
            val: 1
            # List of activator IDs to enable or disable (optional)
            activators:
              - "obj_id_act1"
              - "obj_id_act2"
            # List of door IDs to enable or disable (optional)
            doors:
              - "obj_id_door1"
              - "obj_id_door2"
            # List of light IDs to enable or disable (optional)
            lights:
              - "obj_id_light1"
              - "obj_id_light2"
            # List of script IDs to check objects for
            withScript:
              - "SomeScript_1"
              - "SomeScript_2"
              - "SomeOtherScript_3"
    ```

    3. Just check for a script after a quest update (same as above but we don't care about anything _in_ the script, just if it's running or not):

    ```yaml
    - mod: "Your Mod Name Here"
      plugin: "Your Mod.esp"
      fixes:
        startup: 
          # ...Stuff...
        update: # NOTE this "update" key is important!
          - questId: "Quest_ID_Here"
            # Stage of the quest to check for
            stage: 15
            # Cell X/Y where this should happen
            cellX: -123
            cellY: 23
            # Enable or disable  the thing/things
            enabled: false
            # The script to look for after the given quest stage
            script: "Script_ID_Here"
            # List of activator IDs to enable or disable (optional)
            activators:
              - "obj_id_act1"
              - "obj_id_act2"
            # List of door IDs to enable or disable (optional)
            doors:
              - "obj_id_door1"
              - "obj_id_door2"
            # List of light IDs to enable or disable (optional)
            lights:
              - "obj_id_light1"
              - "obj_id_light2"
            # List of script IDs to check objects for
            withScript:
              - "SomeScript_1"
              - "SomeScript_2"
              - "SomeOtherScript_3"
    ```

    4. Checking a global variable and its value after a quest update:

    ```yaml
    - mod: "Your Mod Name Here"
      plugin: "Your Mod.esp"
      fixes:
        startup: 
          # ...Stuff...
        update: # NOTE this "update" key is important!
          - questId: "Quest_ID_Here"
            # Stage of the quest to check for
            stage: 15
            # Cell X/Y where this should happen
            cellX: -123
            cellY: 23
            # Enable or disable  the thing/things
            enabled: false
            # The script to look for after the given quest stage
            globalVar: 
              key: "GlobalVarHere"
              value: 42 # Or any other value you need, quote strings as needed!
            # List of activator IDs to enable or disable (optional)
            activators:
              - "obj_id_act1"
              - "obj_id_act2"
            # List of door IDs to enable or disable (optional)
            doors:
              - "obj_id_door1"
              - "obj_id_door2"
            # List of light IDs to enable or disable (optional)
            lights:
              - "obj_id_light1"
              - "obj_id_light2"
            # List of script IDs to check objects for
            withScript:
              - "SomeScript_1"
              - "SomeScript_2"
              - "SomeOtherScript_3"
    ```

    5. Just enable/disable after a quest update, no other checks are needed:

    ```yaml
    - mod: "Your Mod Name Here"
      plugin: "Your Mod.esp"
      fixes:
        startup: 
          # ...Stuff...
        update: # NOTE this "update" key is important!
          - questId: "Quest_ID_Here"
            # Stage of the quest to check for
            stage: 15
            # Cell X/Y where this should happen
            cellX: -123
            cellY: 23
            # Enable or disable  the thing/things
            enabled: false
            # List of activator IDs to enable or disable (optional)
            activators:
              - "obj_id_act1"
              - "obj_id_act2"
            # List of door IDs to enable or disable (optional)
            doors:
              - "obj_id_door1"
              - "obj_id_door2"
            # List of light IDs to enable or disable (optional)
            lights:
              - "obj_id_light1"
              - "obj_id_light2"
            # List of script IDs to check objects for
            withScript:
              - "SomeScript_1"
              - "SomeScript_2"
              - "SomeOtherScript_3"
    ```

1. If bad data is provided a log entry will be emitted.

#### Installation

[This short video](https://www.youtube.com/watch?v=xzq_ksVuRgc) describes the process of installing mods with OpenMW-Launcher.

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/distant-fixes-lua-edition/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Patches\DistantFixesLuaEdition

        # Linux
        /home/username/games/OpenMWMods/Patches/DistantFixesLuaEdition

        # macOS
        /Users/username/games/OpenMWMods/Patches/DistantFixesLuaEdition

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Patches\DistantFixesLuaEdition"`)
1. Add `content=distant-fixes-lua-edition.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Configuration

You can access this mod's configuration via the script settings menu, found at: ESC >> Options >> Scripts >> Distant Fixes: Lua Edition

* **Debug Messages** will print log messages to the F10 console when enabled (default: **No**)

#### Lua Console Commands

##### `ShowCompleted`

Print completed fixes to the F10 console.

1. Press \` to bring down the console
1. Type `luag` and press enter
1. Type `I.DistantFixes.ShowCompleted()` and press enter
1. Press `F10` to see the log

##### `ShowCurrent`

Print actively running fixes to the F10 console.

1. Press \` to bring down the console
1. Type `luag` and press enter
1. Type `I.DistantFixes.ShowCurrent()` and press enter
1. Press `F10` to see the log

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/distant-fixes-lua-edition/-/issues)
* Email `distant-fixes at modding-openmw dot com`
* Contact the author on Discord: `@johnnyhostile`

#### Planned Features

* Support as much content as possible
